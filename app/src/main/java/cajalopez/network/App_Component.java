package cajalopez.network;

import cajalopez.network.Repository.UserRepository;
import cajalopez.network.Repository.UserRepository_Module;
import dagger.Component;

/**
 * Created by cajalopez on 7/8/17.
 */

@App_Scope
@Component(modules = UserRepository_Module.class)
public interface App_Component
{

    UserRepository ActivateUserRespository();

}
