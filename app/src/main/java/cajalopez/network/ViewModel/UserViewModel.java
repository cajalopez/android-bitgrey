package cajalopez.network.ViewModel;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cajalopez.network.App_Main;
import cajalopez.network.Gps.LocationLiveData;
import cajalopez.network.Model.User;
import cajalopez.network.Repository.UserRepository;
import timber.log.Timber;

/**
 * Created by cajalopez on 7/8/17.
 */

public class UserViewModel extends ViewModel
{
    public MutableLiveData<String> near_user=new MutableLiveData<>();
    private LiveData<List<User>> users;
    private UserRepository userRepository;
    private List<User> userList;

    public void init()
    {
        userRepository = App_Main.getInstance().getUserRepository();
        users = userRepository.getUsers();
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPostExecute(Void aVoid)
            {
                super.onPostExecute(aVoid);

                if(ActivityCompat.checkSelfPermission(App_Main.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(App_Main.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    return;
                }
                else
                {
                    Double current_lar = userRepository.getLocationLiveData().getLat();
                    Double current_lng = userRepository.getLocationLiveData().getLng();
                    LatLng current_latlon= new LatLng(current_lar,current_lng);

                    List<LatLng> latLngList=new ArrayList<>();
                    List<Float> distLocate = new ArrayList<>();

                    for(int i=0;i<userList.size();i++)
                    {
                        LatLng latLng = new LatLng(userList.get(i).address.geo.lng,userList.get(i).address.geo.lat);
                        latLngList.add(i,latLng);
                        distLocate.add(i,distance(current_latlon,latLng));
                    }

                    int minIndex = distLocate.indexOf(Collections.min(distLocate));
                    near_user.setValue(userList.get(minIndex).username);
                }
            }

            @Override
            protected Void doInBackground(Void... voids)
            {

                userList = userRepository.userDao.findDAORAWUsers();
                if(userRepository.userDao.findDAORAWUsers().isEmpty())
                {
                    Timber.e("Cargando usuarios desdel el servicio WEB");
                    userRepository.loadUsers();
                }
                else
                {
                    Timber.e("Usuarios previamente cargados");
                }
                return null;

            }
        }.execute();
    }
    public LiveData<List<User>> getUsers()
    {
        return users;
    }

    public LocationLiveData getLocationLiveData() {
        return userRepository.getLocationLiveData();
    }

    private static float distance(LatLng current, LatLng last){

        Location cL = new Location("");
        cL.setLatitude(current.latitude);
        cL.setLongitude(current.longitude);

        Location lL = new Location("");
        lL.setLatitude(last.latitude);
        lL.setLongitude(last.longitude);

        return lL.distanceTo(cL);
    }

}
