package cajalopez.network;

import android.content.Context;
import dagger.Module;
import dagger.Provides;

/**
 * Created by cajalopez on 7/9/17.
 */

@Module
public class App_ContextModule {

    private Context context;

    public App_ContextModule(Context context)
    {
        this.context = context;
    }

    @Provides
    public Context Context()
    {
     return this.context;
    }

}
