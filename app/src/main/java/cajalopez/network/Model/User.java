package cajalopez.network.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by cajalopez on 7/8/17.
 */

@Entity(tableName = "USER")
public class  User
{
    @PrimaryKey
    public String id;
    public String name;
    public String username;
    @Embedded
    public address address;

    public static class address
    {

        public String street;
        public String suite;
        public String city;
        public String zipcode;
        @Embedded
        public geo geo;

        public static class geo
        {
            public double lat;
            public double lng;
        }
    }

    public String phone;
    public String website;
    @Embedded
    public company company;

    public static class company
    {
        @ColumnInfo(name = "company_name")
        public String name;
        public String catchPhrase;
        public String bs;
    }

}
