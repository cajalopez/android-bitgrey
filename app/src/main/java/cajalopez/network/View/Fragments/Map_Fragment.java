package cajalopez.network.View.Fragments;

import android.Manifest;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import cajalopez.network.R;
import cajalopez.network.Model.User;
import cajalopez.network.ViewModel.UserViewModel;

/**
 * Created by cajalopez on 7/8/17.
 */

public class Map_Fragment extends Fragment implements OnMapReadyCallback, LifecycleRegistryOwner {
    //CORRECT THIS FEATURE
    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private SupportMapFragment mapFragment;
    private View view;
    private UserViewModel model;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(this).get(UserViewModel.class);
        model.init();
        if(ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION))
            {

            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},1);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        final GoogleMap map = googleMap;
        model.getUsers().observe(this, new Observer<List<User>>()
        {
            @Override
            public void onChanged(@Nullable List<User> users) {
                if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    return;
                }
                else
                {
                    map.setMyLocationEnabled(true);
                    if(users.isEmpty())
                    {

                    }
                    else
                    {
                        for(int i=0;i<users.size();i++)
                        {
                            LatLng latLng = new LatLng(users.get(i).address.geo.lng,users.get(i).address.geo.lat);
                            map.addMarker(new MarkerOptions().position(latLng)
                                    .title(users.get(i).username));
                        }
                    }
                }
            }
        });
        model.near_user.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Snackbar mySnackbar = Snackbar.make(view,"El usuario mas cercano es -> "+s, Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setAction("Continuar", new MyUndoListener());
                mySnackbar.show();
            }
        });
    }

    public class MyUndoListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v) {
            // Code to undo the user's last action
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.map_fragment, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}
