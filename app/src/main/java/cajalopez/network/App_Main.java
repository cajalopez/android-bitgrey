package cajalopez.network;

import android.app.Application;

import cajalopez.network.Repository.UserRepository;
import timber.log.Timber;

/**
 * Created by cajalopez on 7/8/17.
 */

public class App_Main extends Application {

    private static App_Main instancie;
    private App_Component component;
    private UserRepository userRepository;
    public static synchronized App_Main getInstance() {
        if(instancie == null) {
            instancie = new App_Main();
        }
        return instancie;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instancie=this;
        Timber.plant(new Timber.DebugTree());

        component = DaggerApp_Component.builder()
                .app_ContextModule(new App_ContextModule(this))
                .build();

        userRepository = component.ActivateUserRespository();

    }


}
