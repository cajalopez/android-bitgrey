package cajalopez.network;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by cajalopez on 7/9/17.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface App_Scope
{

}
