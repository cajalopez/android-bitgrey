package cajalopez.network.Repository.Api;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Named;

import cajalopez.network.App_Scope;
import cajalopez.network.Repository.Persistent_Data.UserDao;
import cajalopez.network.Repository.Persistent_Data.UserDataBase;
import cajalopez.network.Repository.UserRepository;
import cajalopez.network.ViewModel.UserViewModel;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by cajalopez on 7/8/17.
 */

@Module(includes = WebSetup_Module.class)
public class WebService_Module
{

    @App_Scope
    @Provides
    public Retrofit retrofit(OkHttpClient client)
    {
        return new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://jsonplaceholder.typicode.com")
                .build();
    }


}
