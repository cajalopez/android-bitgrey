package cajalopez.network.Repository.Api;

import java.util.List;
import cajalopez.network.Model.User;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by cajalopez on 7/8/17.
 */

public interface WebService
{
    @GET("/users")
    Call<List<User>> getUsersFromRepository();
}
