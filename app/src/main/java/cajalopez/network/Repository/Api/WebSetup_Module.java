package cajalopez.network.Repository.Api;

import android.content.Context;

import java.io.File;

import cajalopez.network.App_ContextModule;
import cajalopez.network.App_Scope;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

/**
 * Created by cajalopez on 7/9/17.
 */

@Module(includes = App_ContextModule.class)
public class WebSetup_Module
{
    @App_Scope
    @Provides
    public HttpLoggingInterceptor loggingInterceptor()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.e(message);
            }
        });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return interceptor;
    }

    @App_Scope
    @Provides
    public File cacheFile(Context context)
    {
        return new File(context.getCacheDir(),"okHttp_Cache");
    }


    @App_Scope
    @Provides
    public Cache cache(File cacheFile)
    {
        return new Cache(cacheFile,10*1000*1000); // -> 10 MegaBytes Cache
    }


    @App_Scope
    @Provides
    public OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor,Cache cache)
    {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .cache(cache)
                .build();
    }


}
