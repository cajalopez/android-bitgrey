package cajalopez.network.Repository.Persistent_Data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import cajalopez.network.Model.User;


/**
 * Created by cajalopez on 7/8/17.
 */

@Database(entities = {User.class}, version = 1)
public abstract class UserDataBase extends RoomDatabase
{
    public abstract UserDao userDao();
}

