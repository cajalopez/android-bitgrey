package cajalopez.network.Repository.Persistent_Data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import cajalopez.network.Model.User;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

/**
 * Created by cajalopez on 7/8/17.
 */

@Dao
public interface UserDao
{
    @Insert(onConflict = IGNORE)
    void insertDAOUsers(List<User> users);

    @Query("SELECT * FROM User")
    LiveData<List<User>> findDAOUsers();

    @Query("SELECT * FROM User")
    List<User> findDAORAWUsers();

}
