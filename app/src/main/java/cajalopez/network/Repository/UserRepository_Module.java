package cajalopez.network.Repository;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import cajalopez.network.App_Scope;
import cajalopez.network.Repository.Api.WebService;
import cajalopez.network.Repository.Api.WebService_Module;
import cajalopez.network.Repository.Persistent_Data.UserDao;
import cajalopez.network.Repository.Persistent_Data.UserDataBase;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by cajalopez on 7/10/17.
 */

@Module(includes = WebService_Module.class)
public class UserRepository_Module {


    @App_Scope
    @Provides
    public WebService webService(Retrofit retrofitService)
    {
        return retrofitService.create(WebService.class);
    }

    @App_Scope
    @Provides
    public UserDao userDao(Context context)
    {
        UserDataBase dataBase = Room.databaseBuilder(context,UserDataBase.class,"User").build();
        return dataBase.userDao();
    }

    @App_Scope
    @Provides
    public UserRepository repository(WebService webService, UserDao userDao)
    {
        UserRepository userRepository= new UserRepository(webService,userDao);
        return userRepository;
    }


}
