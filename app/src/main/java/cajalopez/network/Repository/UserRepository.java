package cajalopez.network.Repository;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import cajalopez.network.App_Main;
import cajalopez.network.Gps.LocationLiveData;
import cajalopez.network.Repository.Api.WebService;
import cajalopez.network.Model.User;
import cajalopez.network.Repository.Persistent_Data.UserDao;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by cajalopez on 7/8/17.
 */

@Singleton
public class UserRepository
{

    public WebService webService;
    public UserDao userDao;
    public LocationLiveData locationLiveData;

    public LocationLiveData getLocationLiveData()
    {
        return locationLiveData;
    }

    @Inject
    public UserRepository(WebService webService, UserDao userDao)
    {
        this.webService=webService;
        this.userDao=userDao;
        locationLiveData = LocationLiveData.get(App_Main.getInstance());

    }

    public LiveData<List<User>> getUsers()
    {
        Timber.e("Obteniendo usuarios de la basa de datos -> USERS");
        return userDao.findDAOUsers();
    }

    public void loadUsers()
    {
        new AsyncTask<Void,Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... voids)
            {
                try
                {
                    Timber.e("Cargando en la base de datos -> USERS");
                    final Response response = webService.getUsersFromRepository().execute();
                    userDao.insertDAOUsers((List<User>) response.body());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

}
